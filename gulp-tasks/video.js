"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import newer from 'gulp-newer';
import browsersync from 'browser-sync';

gulp.task("video", () => {
    return gulp.src(paths.videos.src)
        .pipe(newer(paths.videos.dist))
        .pipe(gulp.dest(paths.videos.dist))
        .on("end", browsersync.reload);
});

import LocomotiveScroll from 'locomotive-scroll';
import IMask from 'imask';
const scroll = new LocomotiveScroll({
    el: document.querySelector('[data-scroll-container]'),
    smooth: true
});
const sections = document.querySelectorAll('section');
const nav = document.querySelector('.header');
const menuButton = nav.querySelector('.header__menu-button');
const menu = nav.querySelector('.header__menu-wrapper');
const anchors = nav.querySelectorAll('.header__menu-list-link');
const logo = nav.querySelector('.header__logo');
const mainSection = document.querySelector('.section_main');
const serviceSection = document.querySelector('.section_service');
const mainButton = mainSection.querySelector('.main__button');
const sectionAbout = document.querySelector('.section_about');
const infoSlide = document.querySelector('.info-slide');
const infoSlideTitle = infoSlide.querySelector('.info-slide__title');
const infoSlideSubtitle = infoSlide.querySelector('.info-slide__subtitle');
const sliderPrevButton = sectionAbout.querySelector('.content-slider__button_prev');
const sliderNextButton = sectionAbout.querySelector('.content-slider__button_next');
const modalWrapper = document.querySelector('.modal__wrapper');
const modal = modalWrapper.querySelector('.modal__body');
const modalButtons = document.querySelectorAll('.trigger-button');
const modalTitleWrapper = modal.querySelector('.modal__title-wrapper');
const modalForm = modal.querySelector('.modal__form');
const modalSubmitButton = modal.querySelector('.modal-form__button');
const modalThxWrapper = modal.querySelector('.modal__thx-wrapper');
const body = document.querySelector('body');
const main = document.getElementById('main');
const service = document.getElementById('service');
const perks = document.getElementById('perks');
const about = document.getElementById('about');
const headerLogoLink = document.querySelector('.header__logo-link');
const footerForm = document.querySelector('.footer-form');
const footerFormSubmit = footerForm.querySelector('.footer-form__button');

const infoSlideValues = [
    {
        title: `<span class="letter"><span>Гибкие</span></span> <span class="letter"><span>цены</span></span>`,
        subtitle: 'Цифры имеют значение. Мы найдем артиста даже, если ваш бюджет ограничен'
    },
    {
        title: '<span class="letter"><span>Опыт и</span></span> <span class="letter"><span>профессионализм</span></span>',
        subtitle: 'Мы давно в ивент-индустрии и умеем решать задачи любого уровня сложности'
    },
    {
        title: '<span class="letter"><span>Все под</span></span> <span class="letter"><span>контролем</span></span>',
        subtitle: 'Мы постоянно на связи с заказчиком, неприятные сюрпризы исключены'
    }
];

const files = [
    [
        {
            source: 'img/artist_vertical-1.webp',
            alter: 'Artist vertical image 1',
            position: 'left',
            orientation: 'vertical'
        },
        {
            source: 'img/artist_horizontal-2.webp',
            alter: 'Artist horizontal image 1',
            text: 'Организовали выступление Николая Баскова на Дне города Екатеринбурга',
            position: 'left',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_horizontal-1.webp',
            alter: 'Artist horizontal image 2',
            position: 'right',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_vertical-2.webp',
            alter: 'Artist vertical image 2',
            position: 'right',
            orientation: 'vertical',
            align: 'left'
        }
    ],
    [
        {
            source: 'img/artist_vertical-3.webp',
            alter: 'Artist vertical image 3',
            position: 'left',
            orientation: 'vertical'
        },
        {
            source: 'img/artist_horizontal-4.webp',
            alter: 'Artist horizontal image 3',
            position: 'left',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_horizontal-3.webp',
            alter: 'Artist horizontal image 4',
            position: 'right',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_vertical-4.webp',
            alter: 'Artist vertical image 4',
            position: 'right',
            orientation: 'vertical',
            align: 'left'
        }
    ],
    [
        {
            source: 'img/artist_vertical-5.webp',
            alter: 'Artist vertical image 5',
            position: 'left',
            orientation: 'vertical'
        },
        {
            source: 'img/artist_horizontal-6.webp',
            alter: 'Artist horizontal image 5',
            position: 'left',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_horizontal-5.webp',
            alter: 'Artist horizontal image 6',
            position: 'right',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_vertical-6.webp',
            alter: 'Artist vertical image 6',
            position: 'right',
            orientation: 'vertical',
            align: 'left'
        }
    ],
    [
        {
            source: 'img/artist_vertical-7.webp',
            alter: 'Artist vertical image 7',
            position: 'left',
            orientation: 'vertical'
        },
        {
            source: 'img/artist_horizontal-7.webp',
            alter: 'Artist horizontal image 7',
            position: 'left',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_horizontal-8.webp',
            alter: 'Artist horizontal image 8',
            position: 'right',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_vertical-8.webp',
            alter: 'Artist vertical image 8',
            position: 'right',
            orientation: 'vertical',
            align: 'left'
        }
    ]
];
const mobileFiles = [
    [
        {
            source: 'img/artist_vertical-1.webp',
            alter: 'Artist vertical image 1',
            position: 'left',
            orientation: 'vertical'
        },
        {
            source: 'img/artist_horizontal-2.webp',
            alter: 'Artist horizontal image 1',
            text: 'Организовали выступление Николая Баскова на Дне города Екатеринбурга',
            position: 'left',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_vertical-2.webp',
            alter: 'Artist vertical image 2',
            position: 'right',
            orientation: 'vertical',
            align: 'left'
        },
        {
            source: 'img/artist_horizontal-1.webp',
            alter: 'Artist horizontal image 2',
            position: 'right',
            orientation: 'horizontal'
        },
    ],
    [
        {
            source: 'img/artist_vertical-3.webp',
            alter: 'Artist vertical image 3',
            position: 'left',
            orientation: 'vertical'
        },
        {
            source: 'img/artist_horizontal-4.webp',
            alter: 'Artist horizontal image 3',
            position: 'left',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_vertical-4.webp',
            alter: 'Artist vertical image 4',
            position: 'right',
            orientation: 'vertical',
            align: 'left'
        },
        {
            source: 'img/artist_horizontal-3.webp',
            alter: 'Artist horizontal image 4',
            position: 'right',
            orientation: 'horizontal'
        },
    ],
    [
        {
            source: 'img/artist_vertical-5.webp',
            alter: 'Artist vertical image 5',
            position: 'left',
            orientation: 'vertical'
        },
        {
            source: 'img/artist_horizontal-6.webp',
            alter: 'Artist horizontal image 5',
            position: 'left',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_vertical-6.webp',
            alter: 'Artist vertical image 6',
            position: 'right',
            orientation: 'vertical',
            align: 'left'
        },
        {
            source: 'img/artist_horizontal-5.webp',
            alter: 'Artist horizontal image 6',
            position: 'right',
            orientation: 'horizontal'
        },
    ],
    [
        {
            source: 'img/artist_vertical-7.webp',
            alter: 'Artist vertical image 7',
            position: 'left',
            orientation: 'vertical'
        },
        {
            source: 'img/artist_horizontal-7.webp',
            alter: 'Artist horizontal image 7',
            position: 'left',
            orientation: 'horizontal'
        },
        {
            source: 'img/artist_vertical-8.webp',
            alter: 'Artist vertical image 8',
            position: 'right',
            orientation: 'vertical',
            align: 'left'
        },
        {
            source: 'img/artist_horizontal-8.webp',
            alter: 'Artist horizontal image 8',
            position: 'right',
            orientation: 'horizontal'
        },
    ]
];

let activeSection;
let isModalVisible = false;

document.addEventListener('DOMContentLoaded', () => {
    activeSection = sections[0];
    setTimeout(function () {
        mainSection.querySelector('.main__title').classList.add('inited');
    }, 300);
    setTimeout(function () {
        mainSection.querySelector('.main__subtitle').classList.add('inited');
    }, 1500);
    setTimeout(function () {
        mainButton.classList.add('inited');
    }, 2500);
});
menuButton.addEventListener('click', () => {
    if (menu.classList.contains('visible')) {
        menu.classList.remove('visible');
        menuButton.style.transform = 'rotate(0)';
    } else {
        menu.classList.add('visible');
        menuButton.style.transform = 'rotate(90deg)';
    }
})
document.addEventListener('click', (e) => {
    if (!e.target.closest('.flex_nowrap.items_center')) {
        menu.classList.remove('visible');
    }
})
modalButtons.forEach(button => {
    button.addEventListener('click', () => showModal());
});
modal.addEventListener('click', (e) => {
    if (e.target.classList.contains('modal__close-button')) {
        hideModal();
    }
});
modalWrapper.addEventListener('click', (e) => {
    if (e.target.classList.contains('modal__outer')) {
        hideModal();
    }
});
headerLogoLink.addEventListener('click', () => scroll.scrollTo(main));

sliderPrevButton.addEventListener('click', () => prevSlide());
sliderNextButton.addEventListener('click', () => nextSlide());

anchors.forEach(anchor => {
    const windowWidth = window.innerWidth;

    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        if (windowWidth < 1025) {
            menu.classList.remove('visible');
            menuButton.style.transform = 'rotate(0)';
        }

        let scrollSection = null;
        let offset = 0;
        switch(anchor.dataset.anchor) {
            case('main'):
                scrollSection = main;
                break;
            case('service'):
                scrollSection = service;
                break;
            case('perks'):
                scrollSection = perks;
                break;
            case('about'):
                scrollSection = about;
                break;
        }
        if (windowWidth <= 480) {
            offset = '-68';
        }

        scroll.scrollTo(scrollSection, {
            offset: offset
        });

    });
});

scroll.on('scroll', (e) => {
    detectActiveSection();
    if ((e.scroll.y > mainSection.offsetHeight*0.5) && !window.secondScreenAnimated) {
        animateSecondScreen();
    }
    if (
        (e.scroll.y > (serviceSection.offsetHeight + mainSection.offsetHeight + (sectionAbout.offsetHeight*0.5))) &&
        !window.lastScreenAnimated) {
            animateLastScreen();
    }
});

function animateSecondScreen() {
    const service_wrap = document.querySelector('.section_inverse');
    const elems = service_wrap.querySelectorAll('.info__text > span > span');
    const titles = document.querySelectorAll('.action__info-wrapper h4');
    for (let i=0;i<elems.length;i++) {
       (function(ind) {
           setTimeout(function(){elems[ind].style.transform = 'none';}, 15 * ind);
       })(i);
    }
    for (let i=0;i<titles.length;i++) {
        titles[i].style.transform = 'rotate(-90deg)';
    }
    setTimeout(function () {
        service_wrap.classList.add('inited');
    }, 500);
    window.secondScreenAnimated = true;
}
function animateLastScreen () {
    const footerTitle = document.querySelector('.footer__title ');
    setTimeout(function () {
        footerFormSubmit.classList.add('inited');
        footerTitle.classList.add('inited');
    }, 500);
    const elems = document.querySelectorAll('.footer__text > span > span');
    for (let i=0;i<elems.length;i++) {
       (function(ind) {
           setTimeout(function(){elems[ind].style.transform = 'none';}, 15 * ind);
       })(i);
    }
    window.lastScreenAnimated = true;
}
modalSubmitButton.addEventListener('click', (e) => {
   e.preventDefault();
   modalForm.classList.remove('visible');
   modalTitleWrapper.classList.remove('visible');
   modalThxWrapper.classList.add('visible');
});

footerFormSubmit.addEventListener('click', (e) => {
    e.preventDefault();

    modalTitleWrapper.classList.remove('visible');
    modalForm.classList.remove('visible');
    modalThxWrapper.classList.add('visible');
    modalWrapper.classList.add('visible');
    isModalVisible = true;
    body.classList.add('blocked');
})

function initMasks() {
    const footerPhone = document.getElementById('footer-phone');
    const modalPhone = document.getElementById('modal-phone');
    const maskOptions = {
        mask: '+{7}(000)000-00-00'
    };
    IMask(footerPhone, maskOptions);
    IMask(modalPhone, maskOptions);
}

initMasks();

function showModal() {
    modalTitleWrapper.classList.add('visible');
    modalForm.classList.add('visible');
    modalThxWrapper.classList.remove('visible');
    modalWrapper.classList.add('visible');
    isModalVisible = true;
    body.classList.add('blocked');
    setTimeout(function () {
        modalSubmitButton.classList.add('inited');
    }, 300);
}

function hideModal() {
    modalThxWrapper.classList.remove('visible');
    modalTitleWrapper.classList.add('visible');
    modalForm.classList.add('visible');
    modalWrapper.classList.remove('visible');
    isModalVisible = false;
    body.classList.remove('blocked');
    modalSubmitButton.classList.remove('inited');
}

function detectActiveSection() {
    const sectionsArray = Array.from(sections),
        activeSectionIndex = sectionsArray.indexOf(activeSection);

    for (let i = 0; i < sections.length; i++) {
        const position = sections[i].getBoundingClientRect().y,
            elementHeight = sections[i].offsetHeight;
        if (position < 70 && -position < elementHeight) {
            activeSection = sections[i];
        }
    }

    const newIndex = sectionsArray.indexOf(activeSection);

    if (newIndex !== activeSectionIndex) {
        inverseHeader(newIndex);
        // setReelPosition(newIndex);
    }
}

function inverseHeader(index) {
    nav.classList.remove('black');
    anchors.forEach(anchor => anchor.classList.remove('black'));
    if (sections[index].classList.contains('section_inverse')) {
        anchors.forEach(anchor => anchor.classList.add('inverse'));
        logo.innerHTML = `<use xlink:href="img/sprites/sprite.svg#logo_inverse"></use>`
        nav.classList.add('inverse');
    } else if (sections[index].classList.contains('section_about')) {
        anchors.forEach(anchor => anchor.classList.add('inverse'));
        logo.innerHTML = `<use xlink:href="img/sprites/sprite.svg#logo_inverse"></use>`
        nav.classList.add('inverse');
    } else if (sections[index].classList.contains('section_footer')) {
        anchors.forEach(anchor => anchor.classList.remove('inverse'));
        logo.innerHTML = `<use xlink:href="img/sprites/sprite.svg#logo"></use>`;
        nav.classList.remove('inverse');
        nav.classList.add('black');
    } else {
        anchors.forEach(anchor => anchor.classList.remove('inverse'));
        logo.innerHTML = `<use xlink:href="img/sprites/sprite.svg#logo"></use>`;
        nav.classList.remove('inverse');
    }
}

function sectionsAnimation() {
    const style = 'transform: translateY(110%) rotate(0.15deg); transition: transform 1.5s cubic-bezier(0.19, 1, 0.22, 1) 0.1s;';
    const elem = document.querySelector('.section_service .info__text');
    elem.innerHTML = elem
        .textContent
        .split(' ')
        .map(s => {
            if (!s) {
                return '';
            }
            return `<span class="letter"><span style="${style}">${s}</span></span>`;
        })
        .join(' ');
    const elem2 = document.querySelector('.footer__text');
    elem2.innerHTML = elem2
        .textContent
        .split(' ')
        .map(s => {
            if (!s) {
                return '';
            }
            return `<span class="letter"><span style="${style}">${s}</span></span>`;
        })
        .join(' ');
}

sectionsAnimation();


let activeSlide = 0;

function initSlider(activeSlide) {
    sliderPrevButton.classList.remove('disable');
    sliderNextButton.classList.remove('disable');
    if (activeSlide + 1 === files.length) {
        sliderNextButton.classList.add('disable');
    }
    if (activeSlide === 0) {
        sliderPrevButton.classList.add('disable');
    }
    const wrappers = sectionAbout.querySelectorAll('.about__photo-cover');
    wrappers.forEach(item => {
        files[activeSlide].forEach(slide => {
            if (slide.position === item.dataset.position && slide.orientation === item.dataset.orientation) {
                const image = document.createElement('img');
                image.src = slide.source;
                image.classList.add('slider__artist-image')
                item.append(image);

                if (slide.text) {
                    const text = document.createElement('span');
                    text.classList.add('about__photo-cover-text');
                    text.innerText = slide.text;

                    item.append(text);
                }
            }
            item.addEventListener('mouseenter', () => {
                const text = item.querySelector('.about__photo-cover-text');
                // const siblings = item.parentElement.querySelectorAll('.about__photo-cover');
                // for (let i=0;i<siblings.length;i++) {
                //     siblings[i].classList.add('hover');
                // }
                if (text) {
                    text.classList.add('active');
                }
            });
            item.addEventListener('mouseleave', () => {
                const text = item.querySelector('.about__photo-cover-text');
                // const siblings = item.parentElement.querySelectorAll('.about__photo-cover');
                // for (let i=0;i<siblings.length;i++) {
                //     siblings[i].classList.remove('hover');
                // }
                if (text) {
                    text.classList.remove('active');
                }
            })
        });
        item.classList.remove('destroy');
        item.classList.add('init');
    });
}

initSlider(activeSlide);




function destroySlider() {
    const wrappers = sectionAbout.querySelectorAll('.about__photo-cover');
    wrappers.forEach(item => {
        item.classList.remove('init');
        item.classList.add('destroy');
        setTimeout(() => {
            item.innerHTML = '';
        }, 1000);
    })
}



let activeMobileSlide = 0;
let images = [];
function initMobileSlider() {
    mobileFiles.forEach(file => {
        if (Array.isArray(file)) {
            file.forEach(openedFile => {
                images.push(openedFile);
            })
        }
    });
    const imagesWrapper = document.querySelector('.content-slide');
    const image = document.createElement('img');
    image.classList.add('content-slide__image');
    image.src = images[activeMobileSlide].source;
    imagesWrapper.append(image);


    const text = document.createElement('span');
    text.classList.add('about__photo-cover-text');
    if (images[activeMobileSlide].text) {
        text.style.display = 'flex';
        text.innerText = images[activeMobileSlide].text;
    } else {
        text.style.display = 'none';
    }
    imagesWrapper.append(text);
}
if (window.innerWidth < 1025) {
    initMobileSlider();
}

function toggleMobileSlide(index) {
    const image = document.querySelector('.content-slide__image');
    const textEl = image.parentNode.querySelector('.about__photo-cover-text');
    image.src = images[index].source;
    let text = '';
    if (images[index].text) {
        textEl.style.display = 'flex';
        text = images[index].text;
    } else {
        textEl.style.display = 'none';
    }
    textEl.innerText = text;
    sliderPrevButton.classList.remove('disable');
    sliderNextButton.classList.remove('disable');
    if (index + 1 === images.length) {
        sliderNextButton.classList.add('disable');
    }
    if (index === 0) {
        sliderPrevButton.classList.add('disable');
    }
}

function prevSlide() {
    if (window.innerWidth < 1025) {
        if (activeMobileSlide !== 0) {
            activeMobileSlide--;
            toggleMobileSlide(activeMobileSlide);
        }
    } else {
        if (activeSlide !== 0) {
            activeSlide--;
            destroySlider();
            setTimeout(() => {
                initSlider(activeSlide);
            }, 1100);
        }
    }
}

function nextSlide() {
    if (window.innerWidth < 1025) {
        if (activeMobileSlide < images.length - 1) {
            activeMobileSlide++;
            toggleMobileSlide(activeMobileSlide);
        }
    } else {
        if (activeSlide < files.length - 1) {
            activeSlide++;
            destroySlider();
            setTimeout(() => {
                initSlider(activeSlide);
            }, 1100)
        }
    }
}

let activeIndex = 0;
function changeInfoSlide() {
    infoSlideTitle.innerHTML = infoSlideValues[activeIndex].title;
    infoSlideSubtitle.innerText = infoSlideValues[activeIndex].subtitle;
    infoSlideTitle.classList.remove('active');
    infoSlideSubtitle.classList.remove('active');

    const style = 'transform: translateY(110%) rotate(0.15deg); transition: transform 1.5s cubic-bezier(0.19, 1, 0.22, 1) 0.1s;';
    infoSlideSubtitle.innerHTML = infoSlideSubtitle
        .textContent
        .split(' ')
        .map(s => {
            if (!s) {
                return '';
            }
            return `<span class="letter"><span style="${style}">${s}</span></span>`;
        })
        .join(' ');

    setTimeout(function () {
       infoSlideTitle.classList.add('active');
    }, 300);
    setTimeout(function () {
       infoSlideSubtitle.classList.add('active');
    }, 600);

    if (activeIndex === infoSlideValues.length -1) {
        activeIndex = 0;
    } else {
        activeIndex++;
    }
}
changeInfoSlide();
setInterval(() => changeInfoSlide(), 4000);

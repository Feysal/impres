"use strict";

import gulp from "gulp";

const requireDir = require("require-dir"),
    paths = {
        views: {
            src: [
                "./src/views/**/*.html",
            ],
            dist: "./public/",
            watch: [
                "./src/blocks/**/*.html",
                "./src/views/**/*.html"
            ]
        },
        styles: {
            src: "./src/styles/*.{scss,sass}",
            dist: "./public/styles/",
            watch: [
                "./src/blocks/**/*.{scss,sass}",
                "./src/styles/**/*.{scss,sass}"
            ]
        },
        scripts: {
            src: "./src/js/index.js",
            dist: "./public/js/",
            watch: [
                "./src/blocks/**/*.js",
                "./src/js/**/*.js"
            ]
        },
        videos: {
            src: "./src/video/*",
            dist: "./public/video",
            watch: "./src/video/*"
        },
        images: {
            src: [
                "./src/img/**/*.{jpg,jpeg,png,gif,tiff,svg}",
                "!./src/img/favicon/*.{jpg,jpeg,png,gif,tiff}"
            ],
            dist: "./public/img/",
            watch: "./src/img/**/*.{jpg,jpeg,png,gif,svg,tiff}"
        },
        sprites: {
            src: "./src/img/svg/*.svg",
            dist: "./public/img/sprites/",
            watch: "./src/img/svg/*.svg"
        },
        files: {
            src: "./src/files/*.pdf",
            dist: "./public/files/",
            watch: "./src/files/*.pdf"
        },
        fonts: {
            src: "./src/fonts/**/*.{woff,woff2}",
            dist: "./public/fonts/",
            watch: "./src/fonts/**/*.{woff,woff2}"
        },
        favicons: {
            src: "./src/img/favicon/*.{jpg,jpeg,png,gif}",
            dist: "./public/img/favicons/",
        },
        gzip: {
            src: "./src/.htaccess",
            dist: "./public/"
        }
    };

requireDir("./gulp-tasks/");

export { paths };

export const development = gulp.series("clean",
    gulp.parallel(["views", "styles", "scripts", "files", "images", "video", "webp", "sprites", "fonts", "favicons"]),
    gulp.parallel("serve"));

export const prod = gulp.series("clean",
    gulp.parallel(["views", "styles", "scripts", "files", "images", "video", "webp", "sprites", "fonts", "favicons", "gzip"]));

export default development;
